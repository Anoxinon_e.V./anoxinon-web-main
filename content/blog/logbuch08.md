+++
date = "2022-12-18T12:00:00+01:00"
draft = false
author = "Anoxinon"
title = "Logbuch #08"
description = "Aktuelles"
categories = ["allgemeines", "verein", "dienste"]
tags = ["Vereinsangelegenheiten", "Verein"]

+++

###### Die Jahre 2021 & 2022

Hallo zusammen!

In diesem Blogbeitrag wollen wir euch etwas über die letzten zwei
Jahre aus unserem Vereinsleben erzählen.

Das Jahr 2021 war für uns alle immer noch etwas ungewöhnlich. Auf
der einen Seite die Hoffnung auf Normalität, auf der anderen
Seite die täglichen Herausforderungen.

---

###### Vereinstreffen

Dennoch war es uns möglich, Anfang Oktober 2021 ein
vereinsinternes Wochenende zu planen und durchzuführen. Das Ziel
war neben zahlreichen Vorträgen, sich auch persönlich
kennenzulernen. Wie bei allen anderen Vereinen stecken hinter
Anoxinon eben Menschen. Ein persönliches Kennenlernen macht nicht
nur Spaß, sondern hilft auch für das zukünftige miteinander in
der digitalen Welt.

---

###### Technik und die Redaktion

Auch im letzten Jahr war unser Team aus der Redaktion und Technik
wieder fleissig. Zahlreiche Artikel zu Datenschutz,
Datensicherheit wurden auf Anoxinon Media veröffentlicht und im
Wiki-Bereich eingestellt. https://anoxinon.media/

Das ganze Jahr haben wir die Arbeiten der XSF unterstützt, indem
wir die monatlichen Newsletter der XSF zu XMPP in die deutsche
Sprache übersetzten.

Es gab Artikel zu F-Droid App-Store, einer Google Play Store
Alternative. Eine lesenswerte Artikelserie zu sicheren Logins und
Fernsteuerung mit Xpra. Im Jahr 2022 haben wir vier weitere
Artikel zu OpenPGP veröffentlicht. Die Serie aus insgesamt 6
Artikeln gibt einen Einblick zur Verschlüsselung von Dateien und
E-Mails.

Anfang 2022 haben wir uns einen neuen Server besorgt.
Ausgestattet ist dieser mit einer Ryzen 3700x CPU, 64 GB RAM und
2x 1 TB NVMEs sowie 2x 12 TB HDDs. Dieser Server wird in Zukunft
alle Dienste von Anoxinon bereitstellen.  Als Distribution auf
dem Server selbst setzen wir Proxmox ein, um so weitgehendst wie
möglich die Dienste in LXC zu Isolieren und nur in Ausnahmefällen
auf verschwenderische QEMU VMs zurückgreifen zu müssen.

Die gesamten Systeme werden via Zabbix Monitoring überwacht.
Ausserdem wurde die Statusseite überarbeitet:
https://status.anoxinon.de

Als Twitter dann doch den Inhaber wechselte, mussten wir schnell
reagieren. Unsere Social Media Instanz wurde von unserer Technik
auf den neuen Server umgezogen. Aktuell haben wir für unsere
Mastodon Instanz vier Moderatoren im Einsatz.

Wie funktioniert die Teamarbeit bei solch einem Einsatz? Die
Techniker treffen sich in einem Mumble Raum (Audiochatsystem).
Hier wird dann der vorher geplante Einsatz zusammen durchgeführt.
Unser Ziel ist alles im Team zu erarbeiten. Der Verein hat auch
einen Account bei senfcall, bei dem wir Präsentation /
Desktopsharing nutzen könnten. In der Regel werden jedoch die
meisten Dinge vorher im XMPP Raum besprochen und dann zu einem
Termin im Mumble umgesetzt.

Im Januar 2022 haben wir den Fileupload von Anoxinon Messenger
(unserem XMPP Chatsystem) überarbeitet. Der Hauptgrund war die
Verwendung von Quota (lateinisch „wie viel“), dem zugewiesenen
Festplattenspeicherplatze pro Benutzer um Bilder / Videos /
Dateien im Chat zu verwenden.

Ausserdem haben wir als neuen Dienst ein IP Lookup bereitgestellt.

---

###### Support FLOSS mittels Spenden

Zum Jahresende 2021 haben wir insgesamt 1.000,00 Euro an folgende
Projekte gespendet. An das F-Droid Projekt und Andreas Rehberg
(Izzy) für eine Alternative zum Google Playstore, an The Document
Foundation (LibreOffice), an den Verein Digitalcourage e.V.,
sowie an Open Knowledge Foundation Deutschland, ein Verein der
sich unter anderem für Projekte zu Informationsfreiheit einsetzt.

---

###### Anoxinon

Nette Unterhaltungen begleiten unseren Alltag in unseren XMPP
Gruppenchats. Fragen zum Verein, Datenschutz, freie Software aber
auch alltägliche Themen finden hier ihren Platz.

Solltet ihr Interesse an unserem Verein haben, seid ihr
eingeladen uns in unserem öffentlichen Gruppenchat zu besuchen:

xmpp:anoxinon@conference.anoxinon.me?join

Anfang 2021 haben wir im Verein Nachwuchs bekommen. Stefans
zweite Tochter wurde geboren. Aktuell hat der Verein 18 aktive
Mitglieder und 5 Fördermitglieder. Dazu noch eine Reihe
ehrenamtlich Tätiger. An dieser Stelle ganz herlichen Dank für
Eure Unterstützung und Mitarbeit!

###### Finanzen

Der Verein finanziert sich durch die Mitgliedsbeiträge und Spenden.

Die monatlichen Kosten belaufen sich auf ca. 205 €. Das
monatliche Beitragsaufkommen beläuft sich auf 268 €.

###### Wie geht es weiter?

In der Technik werden wir noch alle weiteren Dienste auf Proxmox
umstellen. Es soll eine Artikelserie zu dem Messengersystem XMPP
geben und weitere Artikel.

---

Viele Grüße

Euer Anoxinon Team - für eine gemeinschaftliche, digitale Welt.
