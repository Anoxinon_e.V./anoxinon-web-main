--- 
date: "2024-03-07T20:00:00+01:00" 
draft: false
author: "Anoxinon" 
title: "Kurze Info zu 2023" 
description: "Infos zum Verein" 
categories:
- "Community"
tags:
---

Hallo zusammen!

Auch dieses Jahr fand am 27.02. die ordentliche Mitgliederversammlung des Vereins statt. An der Besetzung des Vorstandes hat sich nichts geändert und es sind immer noch die [selben fünf](https://anoxinon.de/ueber-uns/) mit an Bord. 

Ausser unserer IT-Infrastruktur gab es letztes Jahr keine nennenswerten Neuerungen bzw. Änderungen. Wir haben uns für leistungsfähigere Hardware entschieden (entscheiden müssen eher), da Mastodon die alte Kiste ans Limit gebracht hatte. Inzwischen läuft alles soweit reibungslos und die letzten Dienste samt Homepage werden die nächste Zeit ebenfalls noch migriert.

Aktuell hat der Verein gesamt 23 Mitglieder. Wir suchen nach wie vor gerne Verstärkung vor allem im Bereich Media für Artikel und dergleichen. Aber jede(r) ist herzlich willkommen, egal in welchem Bereich. 

Finanziell geht es dem Verein auch sehr gut. Wir machen durch den neuen Server zwar ca. monatlich 60 € Minus aber wir haben noch genug Rücklagen. Daher ist die Finanzierung für lange Zeit gesichert. 

Danke an alle die uns unterstützen!