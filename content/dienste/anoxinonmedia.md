+++
description = "Content Plattform"
title = "Anoxinon Media"
draft = false
weight = 2
bref="Content Plattform des Anoxinon e.V"
toc = true
+++

### Was ist Anoxinon Media?

Anoxinon Media ist die Plattform des Vereins, um der Allgemeinheit Themen wie Datensicherheit, Verschlüsselung, Datenschutz, Zensur, freie Software und andere digitale Thematiken einfach und verständlich zugänglich zu machen. Wir versuchen dabei in regelmäßigen Abständen Inhalte für verschiedene Zeilgruppen - von Einsteiger:innen bis zu Profis - zu veröffentlichen. Klicke dich am Besten einfach mal durch!

<a class="btn-mini" href="https://anoxinon.media">Zu Anoxinon Media</a> | <a class="btn-mini" href="https://social.anoxinon.de/@anoxinonmedia">Anoxinon Media auf Mastodon</a>

### Übersichtsecke

Die Angebote von Anoxinon Media sind alle auf der oben verlinkten Website zentriert. Dennoch gibt es verschiedene Rubriken, auf die an der Stelle hingewiesen werden soll:

* [Der Anoxinon Media Blog](https://anoxinon.media)
* [Anxicon - Das Anoxinon Media Lexikon](https://anoxinon.media/anxicon/)
* [Anoxinon Media Flyer](https://anoxinon.media/downloads/)
* [Die von Anxoinon Media empfohlenen Programme](https://anoxinon.media/empfohleneprogramme/)
* [Die von Anoxinon Media empfohlenen Dienste](https://anoxinon.media/empfohlenedienste/)
* [Von Anoxinon Media empfohlene Inhalte (Bücher/Filme/Studien usw...)](https://anoxinon.media/weitereempfehlungen/)
