--- 
date: "2021-05-10T19:30:00+01:00" 
author: "Anoxinon" 
title: "XMPP Newsletter April 2021" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den April 2021" 
categories: 
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/04/newsletter-04-april/). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter für den Monat April 2021.

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Freiwilligen. Wenn Du mit den Diensten und der Software, die Du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, Dich zu bedanken oder diesen Projekten zu helfen!

Ließ diesen Newsletter als RSS-Feed:

- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht:

- Englisch (Original) auf [xmpp.org](https://xmpp.org/category/newsletter.html)
- Französisch auf [linuxfr.org](https://linuxfr.org/tags/xmpp/public) und [jabberfr.org](https://news.jabberfr.org/category/newsletter/)
- Die italienische Version des letzten Monats auf [NicFab.it](https://www.nicfab.it/la-newsletter-xmpp-di-marzo-2021-versione-italiana/)

## Ankündigungen

Die XSF-Foundation ist nun [auf YouTube [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)! Du kannst dort Aufnahmen von Konferenzen, den XMPP Arbeitsstunden und viele andere unterhaltsame Inhalte finden.

Du hälst einen Vortrag oder veranstaltest eine Diskussion über XMPP-verwandte Technologien? Die XMPP Arbeitsstunden sind wöchentliche Konferenzen von XMPP-Enthusiast:innen für den Austausch und Präsentationen. Für mehr Informationen oder für die Anmeldung kannst Du die entsprechende [Wiki Seite [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) besuchen.

## Veranstaltungen

[XMPP Arbeitsstunden jede Woche [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) - Schau außerdem gerne auf unserem neuen [YouTube-Kanal](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA) vorbei!

[Berlin XMPP Meetup (virtuell) [EN]](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Das monatliche Treffen von XMPP-Enthusiast:innen in Berlin, an jedem zweiten Mittwoch eines jeden Monats.

## Artikel

Im Mai 2021 gab es eine große inhaltliche Aktualisierung von [freie-messenger.de [DE]](https://www.freie-messenger.de/). Ab sofort sind die Inhalte internationalisiert. Als erster Schritt ist die [Schnellübersicht von Messengern [DE]](https://www.freie-messenger.de/systemvergleich/#schnell%C3%BCbersicht) in Deutsch und Englisch verfügbar. Freiwillige sind zur Mithilfe aufgerufen!

[Isode präsentiert deren militärische Fähigkeiten auf der DESI 2021 [EN]](https://www.isode.com/company/wordpress/dsei-2021-demo/) und bietet in diesem Zuge Instant Messaging für das Militär und mehr.

[Gajim's News aus der Entwicklung für den April 2021 [DE]](https://gajim.org/de/post/2021-04-30-development-news-april/): Im April veröffentlichte man einen neuen Styling-Parser für Nachrichten, wodurch Gajim vollständig kompatibel mit XEP-0393 wird. Der Artikel gibt ebenso einen Einblick in die über die vergangenen Monate entwickelten Funktionen: Die neue Chat-Ansicht und das neue Fenster der Kontakt-Info.

![Gajim's Chat-Ansicht](/img/blog/xmpp_newsletter_april_2021/gajim_chat-view-april.png)

Libervia (bisher genannt Salut à Toi) wurde für eine finanzielle Unterstützung durch den NLNet/NGI0 Discovery Fund (der finanzielle Unterstützung des Next Generation Internet Programms von der europäischen Kommision erhält) für ein [ActivityPub <=> XMPP Gateway, gepaart mit einem Pubsub Ende-zu-Ende-Verschlüsselungs-Projekt [EN]](https://www.goffi.org/b/activitypub-gateway-and-pubsub-e2ee-QGqK) ausgewählt. Das XMPP <=> ActivityPub Gateway wird zwei wesentliche dezentralisierte Protokolle verbinden. In der Praxis wird es eine XMPP-Server-Komponente (nutzbar mit jedem Server) geben, die das ActivityPub Server-zu-Server Protokoll (auch "Federation Protocol") implementiert. Auf XMPP-Seite wird das hauptsächlich durch einen PubSub-Dienst umgesetzt werden (mit einigen Extras, wie die Konvertierung von privaten Nachrichten zu einer XMPP-Message-Stanza).

[Vaxbot wächst weiter [EN]](https://monal.im/blog/vaxbot-continues-to-grow/): VaxBot ist ein freier Dienst, der Dir hilft, einen Impftermin in Deinem Bundesstaat [Anmerkung: In den USA] zu finden. In den letzten Wochen ist [Vaxbot [EN]](https://vaccine.monal.im) weiter gewachsen. Der Dienst hat die 8 Millionen gesendeten Nachrichten geknackt und sendet aktuell rund 800 000 Nachrichten am Tag. Über XMPP und Vaxbot wurde in den Fernsehnachrichten in Las Vegas und South Carolina, im Radio in Washington und in Zeitungen in vielen Bundesstaaten berichtet.

In Bezug auf die Vaxbot-Nachrichten: Georg Lukas, der den Server hinter Vaxbot betreibt, hat einen Artikel über die Herausforderungen bezüglich der steigenden Abonnements geschrieben und wie der Dientst die Infrastruktur anpassen musste, um [mit der neuen VaxBox Leistungs-Herausforderung schrittzuhalten [EN]](https://yaxim.org/blog/2021/04/09/vaxbot-performance-challenge/). 

![Server-Registrationen bei Yaxim](/img/blog/xmpp_newsletter_april_2021/yaxim.png)  

Snikket hat bekanntgegeben, dass die [XMPP-Konten-Übertragbarkeit [EN]](https://snikket.org/blog/dapsi-fund-account-portability/) von der [NGI DAPSI [EN]](https://dapsi.ngi.eu/) Initiative getragen wird. Sie wird die benötigten Standards (über [XEP-0227 [EN]](https://xmpp.org/extensions/xep-0227.html) und [XEP-0238 [EN]](https://xmpp.org/extensions/xep-0283.html) hinausgehend), die quelloffenen Werkzeuge und die Integration der Funktionalität in Snikket umfassen.

Nicola Fabiano hat einen Artikel mit dem Titel ["Whatsapp? No, thanks, I prefer to have control over my data [EN]](https://www.nicfab.it/whatsapp-no-thanks-i-prefer-to-have-control-over-my-data/) veröffentlicht.

## Software News

### Clients und Apps

[Conversations 2.9.11 und Conversations 2.9.12 wurden herausgegeben [EN]](https://github.com/iNPUTmice/Conversations/blob/master/CHANGELOG.md), welche Fehler hinsichtlich 'No Connectivity' / 'Keine Verbindung' auf Android 7.1 beheben und Unterstützung für Roster Pre-Authentication mitbringen.

[Gajim 1.3.2 wurde veröffentlicht [DE]](https://gajim.org/de/post/2021-04-24-gajim-1.3.2-released/): Diese Version bringt Übersetzungen für Windows-Nutzer:innen zurück, inklusive einiger weiterer kleinerer Fehlerbehebungen und Verbesserungen.

[Monal 5 Beta 4 [EN]](https://monal.im/blog/monal-5-beta-4/) wurde mit einer Reihe von Verbesserungen veröffentlicht.

Da über die letzten Jahre viele Alternativen zu [sendxmpp [EN]](https://sendxmpp.hostname.sk/) entstanden sind, gibt es dazu nun eine in Arbeit befindliche Übersicht im [XSF Wiki [EN]](https://wiki.xmpp.org/web/User:MDosch/Sendxmpp_incarnations)

[UWPX ersetzt das SQLite-net DB Backend mit einer new Entity Framework Core DB [EN]](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.31.0.0). Außerdem beinhaltet die neue Version eine Fehlerkorrektur und eine Aktualisierung der OMEMO-Implementierung auf die aktuellste Version des Entwurfs (OMEMO 0.7.0 (2020-09-05)). Diese Version ist inkompatibel mit allen vorherigen Versionen von OMEMO und daher erlaubt es UWPX nun ausschließlich OMEMO-verschlüsselte Nachrichten mit Kontakten auszutauschen, die mindestens den OMEMO 0.7.0 (2020-09-05) Entwurf unterstützen. Nun kann UWPX als Client mit einer ordentlichen Eins zu Eins OMEMO-Implementierung auftreten.

Die [Spark 3.0.0 Beta [EN]](https://discourse.igniterealtime.org/t/spark-3-0-0-beta-released/90084) wurde veröffentlicht und beinhaltet die üblichen Fehlerbehebungen. Darüberhinaus ist nun Pade Meeting eingebaut (was Audio- und Videotelefonie ermöglicht) und all dies unter einer vollständigen Überarbeitung der Benutzeroberfläche.


### Server

[ejabberd 21.04 wurde veröffentlicht [EN]](https://www.process-one.net/blog/ejabberd-21-04/): Das neue ejabberd 21.04 Release enthält viele Fehlerbehebungen und einige Verbesserungen. Diese Version enthält kleinere Verbesserungen zur vollständigen Unterstützung von Erlang/OTP 24 und Rebar3. Gleichzeitig behält es die Unterstützung der alten Versionen von Erlang/OTP 19.3 und Rebar2 bei.

[Openfire 4.6.3 wurde herausgegeben [EN]](https://discourse.igniterealtime.org/t/openfire-4-6-3-is-released/90062): Diese Version enthält eine Reihe von Fehlerkorrekturen und kennzeichnet den Wunsch, stabile Versionen der 4.6.x-Serie bereitzustellen, während die Entwicklungsarbeit an einer 4.7.0-Version fortgesetzt wird.

### Libraries (Bibliotheken)

[slixmpp 1.7.1 [EN]](https://lab.louiz.org/poezio/slixmpp/-/releases/slix-1.7.1) wurde veröffentlicht, bei dem es sich um ein Bugfix-Release für einen selten genutzten Code-Pfad handelt, der an OMEMO gebunden ist.

## Extensions und Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welches die formalen und anerkannten Definitionen für die Types (Typen), States (Status) und Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

- Diesen Monat wurden keine XEPs vorgeschlagen.

### New (Neu)

- Version 1.0.0 von [XEP-0457 [EN]](https://xmpp.org/extensions/xep-0457.html) (Message Fancying)
    - Diese Spezifikation definiert eine Unicode-formatierte Fancy-Text-Syntax zur Verwendung in Instant Messages.
    - Erste veröffentlichte Version. (XEP Editor (jsc))

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

- Diesen Monat wurden keine XEPs zurückgestellt.

### Updated (Aktualisiert)

- Version 0.4.0 der [XEP-0420 [EN]](https://xmpp.org/extensions/xep-0420.html) (Stanza Content Encryption)
    - Namespace wird durch die Verwendung von 'envelope' und 'content' durch Umbenennung der Elemente aktualisiert (melvo)
- Versions 0.4.0 und 0.5.0 der [XEP-0434 [EN]](https://xmpp.org/extensions/xep-0434.html) (Trust Messages (TM))
    - Neuen Abschnitt hinzufügen, präzisere Sätze verwenden, konsistente Formatierung anwenden
    - Neuen Abschnitt für Trust Message URIs hinzufügen
    - Verwendung von 'that' anstelle von 'which' für einschränkende Klauseln
    - Einheitliche Formatierung für Absätze und Revisionshistorie anwenden
    - Update auf XEP-0420 Version 0.4.0 und Anpassung des Namespace an den Kurznamen
    - Ersetzen des alten 'content'-Elements von SCE durch das neue 'envelope'-Element
    - Ersetzen des alten 'payload'-Elements von SCE durch das neue 'content'-Element
    - Aktualisierung des Namespaces von SCE auf 'urn:xmpp:sce:1'
    - Namespace von TM auf 'urn:xmpp:tm:0' ändern (melvo)
- Versions 0.2.0 and 0.3.0 of [XEP-0450 [EN]](https://xmpp.org/extensions/xep-0450.html) (Automatic Trust Management (ATM))
    - Verwendung von Trust Message URIs hinzufügen, präzisere Sätze verwenden, konsistente Formatierung anwenden
    - Verwendung von Trust Message URIs für initiale Authentifizierungen 
    - Verwendung von 'that' anstelle von 'which' für einschränkende Klauseln
    - Einheitliche Formatierung für Absätze und Revisionshistorie anwenden
    - Aktualisierung auf XEP-0420 Version 0.4.0 und XEP-0434 Version 0.5.0
    - Ersetzen des alten 'content'-Elements von SCE durch das neue 'envelope'-Element
    - Ersetzen des alten 'payload'-Elements von SCE durch das neue 'content'-Element
    - Aktualisierung des Namespaces von SCE auf 'urn:xmpp:sce:1'
    - Aktualisierung des Namespaces von TM auf 'urn:xmpp:tm:0'
    - Namespace aktualisieren auf 'urn:xmpp:atm:1' (melvo)

### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Calls gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für die Aufwertung zum Entwurf zurück an den Council geht.

- [XEP-0280 [EN]](https://xmpp.org/extensions/xep-0280.html) Message Carbons
- [XEP-0313 [EN]](https://xmpp.org/extensions/xep-0313.html) Message Archive Management

### Draft (Entwurf)

- Diesen Monat gibt es keine neuen Drafts.

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

- Diesen Monat gab es keinen Call for Experience.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an alkino, emus, Licaon_Kter, im, Martin, mathieui, MattJ, nicola, peetah, Sam, seveso, therealjeybe, wurstsalat and Ysabeau ihre Hilfe, ihn zu erstellen!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Mastodon [EN]](https://fosstodon.org/@xmpp/)
* [YouTube [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [Twitter [EN]](https://twitter.com/xmpp)
* [Reddit [EN]](https://www.reddit.com/r/xmpp/)
* [LinkedIn [EN]](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook [EN]](https://www.facebook.com/jabber/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die montaliche Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.
