--- 
date: "2023-06-06T10:00:00+02:00" 
draft: false
author: "Anoxinon" 
title: "XMPP Newsletter Mai 2023" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den Mai 2023" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2023/06/the-xmpp-newsletter-may-2023/). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter, schön, dass du wieder hier bist! Diese Ausgabe umfasst den Monat Mai 2023. Vielen Dank an alle unsere Leserinnen und Leser und alle Mitwirkenden!

Wie dieser Newsletter sind auch viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft das Ergebnis der freiwilligen Arbeit von Menschen. Wenn du mit den Diensten und der Software, die du nutzt, zufrieden bist, solltest du in Erwägung ziehen, dich zu bedanken oder diese Projekte zu unterstützen! Hast du Interesse, das Newsletter-Team zu unterstützen? Lies weiter unten mehr.

## XSF-Ankündigungen

- Wenn du daran interessiert bist, [XSF-Mitglied](https://xmpp.org/community/membership/) [EN] zu werden, kannst du dich bald im dritten Quartal 2023 bewerben.

## XMPP und Google Summer of Code 2023

Die XSF wurde [erneut als gastgebende Organisation beim GSoC 2023 akzeptiert und erhält zwei Slots für XMPP Contributors](https://xmpp.org/2023/02/xmpp-at-google-summer-of-code-2023/) [EN]!

- Der GSoC-Beitragende für Moxxy hat seinen [ersten Blogbeitrag](https://moxxy.org/posts/2023-05-06-Groupchat-GSoC-Project.html) [EN] veröffentlicht, in dem er den Plan zur Implementierung grundlegender Groupchat-Funktionen beschreibt.

![Logo von XMPP, ein "+" und das Logo vom GSoC 2023](/img/blog/xmpp_newsletter_mai_2023/gsoc-2023-logo.svg)

## XSF Fiskal-Hosting-Projekte

Die XSF bietet [Fiskalhosting](https://xmpp.org/community/fiscalhost/) [EN] für XMPP-Projekte an. Bitte bewirb dich über [Open Collective](https://opencollective.com/xmpp) [EN]. Weitere Informationen findest du im [Blogbeitrag zur Ankündigung](https://xmpp.org/2021/09/the-xsf-as-a-fiscal-host/) [EN]. Aktuelle Projekte:

- [Bifrost-Brücke: Offline-Nachrichtenaustausch zwischen Matrix und XMPP](https://opencollective.com/bifrost-mam) [EN]
- [Mellium Co-op}(https://opencollective.com/mellium) [EN]

## XMPP-Veranstaltungen

- [XMPP Office Hours](https://wiki.xmpp.org/web/XMPP_Office_Hours) [EN]: verfügbar auf unserem [YouTube-Kanal](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA) [EN]
- [Berlin XMPP Meetup (remote)](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup) [EN]: monatliches Treffen von XMPP-Enthusiasten in Berlin, jeden zweiten Mittwoch im Monat
- [FOSSY](https://fossy.us/) [EN] wird diesen Sommer einen XMPP-Track auf ihrer Konferenz vom 13. bis 16. Juli 2023 haben
- [XMPP Italian Happy Hour](https://tube.nicfab.eu/w/85RxLsnNTc9FcdFzFgkdsP) [IT]: monatliches italienisches XMPP-Webmeeting, ab 16. Mai und dann jeden dritten Dienstag im Monat um 19:00 Uhr (Online-Event, mit Webmeeting-Modus und Live-Streaming).

## Talks

- Der Entwickler von Libervia hat für Juni ein paar informative Vorträge in Paris geplant. Die Vorträge werden sich mit verschiedenen Aspekten des Libervia-Projekts und XMPP befassen. Der erste Vortrag ist eine 20-minütige Session auf Englisch und findet am Donnerstag, den 15. Juni um 15:00 Uhr im [OW2](https://www.ow2con.org/view/2023/Abstract_Community_Day#15061500) [EN] statt. Der zweite Vortrag ist eine 60-minütige Diskussion auf Französisch, die am Freitag, den 16. Juni um 17:30 Uhr im [Pas Sage En Seine](https://passageenseine.fr/) [FR] stattfindet. Diese Veranstaltungen bieten eine gute Gelegenheit, Einblicke in das Libervia-Projekt zu bekommen und mit den Entwicklern zu sprechen.

## Artikel

- Die deutsche Bundesregierung hat eine [Studie über Video- und Messaging-Dienste](https://www.bundeskartellamt.de/SharedDocs/Meldung/DE/Pressemitteilungen/2023/17_05_2023_SU_MD.html) veröffentlicht, in der die XSF und auch mehrere XMPP-Projekte Stellungnahmen abgegeben haben. Lies die [Zusammenfassung](https://www.bundeskartellamt.de/SharedDocs/Publikation/DE/Sektoruntersuchungen/Sektoruntersuchung_MessengerVideoDienste_Zusammfassung.pdf?__blob=publicationFile&v=3) oder das [vollständige Dokument](https://www.bundeskartellamt.de/SharedDocs/Publikation/DE/Sektoruntersuchungen/Sektoruntersuchung_MessengerVideoDienste.pdf?__blob=publicationFile&v=5).

## Software Neuigkeiten

### Clients und Anwendungen

- [Gajim 1.8.0](https://gajim.org/post/2023-05-27-gajim-1.8.0-released/) [EN] wurde veröffentlicht und kommt mit integrierter OMEMO-Verschlüsselung! Die Integration des OMEMO-Plugins sorgt für eine engere Integration und ein besseres Nutzererlebnis. Das Chat-Menü wurde neu angeordnet und einige Schnellschaltflächen wurden hinzugefügt, um die Bedienung zu erleichtern. Sowohl die Nachrichtensuche als auch die Gesprächsansicht von Gajim haben einige wichtige Änderungen und Korrekturen erhalten.

![Gajim's Chat-Banner in einem Gruppenchat](/img/blog/xmpp_newsletter_mai_2023/gajim-chat-banner-group-chat.png)

- [Kaidan 0.9 und 0.9.1](https://www.kaidan.im/2023/05/05/kaidan-0.9.0/) [EN] sind veröffentlicht worden! Sie bieten Unterstützung für OMEMO 2, Automatic Trust Management (ATM), [XMPP-Provider](https://providers.xmpp.net/) [EN], Nachrichtenreaktionen und vieles mehr.

![Kaidan's Chat-Ansicht](/img/blog/xmpp_newsletter_mai_2023/kaidan-0.9-horizontal.png)

- Für [Libervia](https://libervia.org/) [EN] zeichnen sich spannende Entwicklungen ab. Dank eines Zuschusses von NLnet über den NGI Assure Fund wird derzeit an der Implementierung von A/V-Anrufen mit Jingle über mehrere Frontends gearbeitet. Diese neue Funktion soll sowohl Einzel- als auch Mehrparteienanrufe unterstützen, und es ist sogar geplant, die Fernsteuerung des Desktops zu ermöglichen. Außerdem wird derzeit das ActivityPub Gateway stabilisiert, das die Funktionalität von Libervia weiter verbessern wird. Eine vollständige Übersicht über diese und weitere Updates findest du in der aktuellen Fortschrittsmitteilung auf [Goffis Blog](https://www.goffi.org/b/libervia-progress-note-2023-w22-x3Wa) [EN].
- Mit JMP kannst du Text- und Bildnachrichten (und Anrufe) über eine echte Telefonnummer direkt von deinem Computer, Tablet, Telefon oder einem anderen Gerät, das über einen Jabber-Client verfügt, senden und empfangen. Jabber-seitige Reaktionen werden jetzt soweit möglich in die Tapback-Pseudo-Syntax übersetzt. Lies mehr im [JMP-Blog](https://blog.jmp.chat/b/april-newsletter-2023) [EN]. [Cheogram Android 2.12.1-6](https://git.singpolyma.net/cheogram-android/refs/2.12.1-6) [EN] bietet Farben und Ruhezeiten pro Account, Thumbhash-Vorschauen für Bilder und viele Fehlerbehebungen.

### Server

- [MongooseIM 6.1.0](https://www.erlang-solutions.com/blog/mongooseim-6-1-handle-more-traffic-consume-less-resources/) [EN] ist veröffentlicht worden.
- Für den Openfire XMPP-Server gab es zwei neue Versionen: [Openfire 4.6.8 (old stable)](https://discourse.igniterealtime.org/t/openfire-4-6-8-release) [EN] und [Openfire 4.7.5 (stable)](https://discourse.igniterealtime.org/t/openfire-4-7-5-release) [EN]. Außerdem wurde ein [CVE zur Umgehung der Authentifizierung in der Openfire-Verwaltungskonsole veröffentlicht](https://discourse.igniterealtime.org/t/cve-2023-32315-openfire-administration-console-authentication-bypass/92869) [EN].

### Bibliotheken & Tools

- [omemo-dr](https://dev.gajim.org/gajim/omemo-dr/) [EN], eine neue OMEMO-Kryptobibliothek, ist verfügbar. *omemo-dr* ist ein Fork von *python-axolotl*, der Kryptobibliothek, die für die OMEMO-Verschlüsselung in Gajim verwendet wird. In Vorbereitung auf zukünftige Änderungen (z. B. die nächste OMEMO-Version) haben die Gajim-Entwickler diese Bibliothek geforkt.
- [python-nbxmpp 4.3.0 und 4.3.1](https://dev.gajim.org/gajim/python-nbxmpp/-/blob/master/ChangeLog) [EN] wurden veröffentlicht und bringen verschiedene Fehlerkorrekturen in Bezug auf OMEMO und HTTP mit sich.

### Erweiterungen und Spezifikationen

Die XMPP Standards Foundation entwickelt neben den [XMPP RFCs](https://xmpp.org/rfcs/) auch Erweiterungen für XMPP in ihrer [XEP-Serie](https://xmpp.org/extensions/).

Entwickler und andere Standardisierungsexperten aus der ganzen Welt arbeiten gemeinsam an diesen Erweiterungen, entwickeln neue Spezifikationen für neue Praktiken und verfeinern bestehende Vorgehensweisen. Die besonders erfolgreichen Vorschläge werden - je nach Art - als Final oder Active veröffentlicht, während andere sorgfältig als Deferred archiviert werden. Dieser Lebenszyklus wird in [XEP-0001](https://xmpp.org/extensions/xep-0001.html)  [EN] beschrieben, der die formalen und kanonischen Definitionen für die Typen, Zustände und Prozesse enthält. [Lies mehr über den Standardisierungsprozess](https://xmpp.org/about/standards-process.html) [EN]. Die Kommunikation über Standards und Erweiterungen erfolgt über die [Mailingliste für Standards](https://mail.jabber.org/mailman/listinfo/standards) ([Online-Archiv](https://mail.jabber.org/pipermail/standards/) [EN]).

### Vorgeschlagene

Der XEP-Entwicklungsprozess beginnt damit, dass du eine Idee aufschreibst und sie dem XMPP-Editor vorlegst. Innerhalb von zwei Wochen entscheidet der Rat, ob dieser Vorschlag als experimenteller XEP angenommen wird.

- Diesen Monat wurden keine XEP vorgeschlagen.

### Neu

- [Version 0.1.0 von XEP-0480 (SASL Upgrade Tasks)](https://xmpp.org/extensions/xep-0480.html)
	- Diese Spezifikation bietet eine Möglichkeit zum Upgrade auf neuere SASL-Mechanismen mit Hilfe von SASL2-Tasks.

- [Version 0.1.0 von XEP-0482 (Anrufeinladungen)](https://xmpp.org/extensions/xep-0482.html)
	- Dieses Dokument definiert, wie man jemanden zu einem Anruf einlädt und wie man auf die Einladung antwortet.

- [Version 0.1.0 von XEP-0481 (Content Types in Messages)](https://xmpp.org/extensions/xep-0481.html)
	- Diese Spezifikation beschreibt ein allgemeines Verfahren, mit dem Inhalte in Nachrichten mit einem bestimmten Internet-Inhaltstyp gekennzeichnet werden können. Außerdem wird eine Methode beschrieben, mit der derselbe Inhalt unter Verwendung verschiedener Inhaltstypen versendet werden kann, um bei der Kommunikation zwischen Clients, die unterschiedliche Inhaltstypen unterstützen, eine Ausweichmöglichkeit zu haben.

- [Version 0.1.0 von XEP-0478 (Stream Limits Advertisement)](https://xmpp.org/extensions/xep-0478.html)
	- Diese Spezifikation legt fest, wie eine XMPP-Entität die Limits ankündigen kann, die sie für die in einem Stream empfangenen Daten durchsetzen wird.

- [Version 0.1.0 von XEP-0479 (XMPP Compliance Suites 2023)](https://xmpp.org/extensions/xep-0479.html)
	- Dieses Dokument definiert XMPP-Anwendungskategorien für verschiedene Anwendungsfälle (Core, Web, IM und Mobile) und legt die erforderlichen XEP fest, die Client- und Serversoftware implementieren müssen, um die Anwendungsfälle zu erfüllen.
	
### Aufgeschoben

Wenn ein experimenteller XEP mehr als zwölf Monate lang nicht aktualisiert wird, wird er von "Experimentell" nach "Zurückgestellt" verschoben. Wenn es eine weitere Aktualisierung gibt, wird der XEP wieder auf "Experimentell" gesetzt.

- Diesen Monat wurden keine XEP zurückgestellt.

### Aktualisiert

- [Version 1.25.0 von XEP-0060 (Publish-Subscribe)](https://xmpp.org/extensions/xep-0060.html)
	- Hinzufügen von Informationstext zum Datenmodell in den Implementierungshinweisen.
	- Bereitstellung einer Möglichkeit für einen PubSub-Dienst, einem Client mitzuteilen, wenn ein Knoten voll ist.
	- Klärung des Verhaltens und der aktuellen Verwendung der Option "pubsub#itemreply".
	- Fix spec approver

- [Version 1.1.1 von XEP-0223 (Persistent Storage of Private Data via PubSub)](https://xmpp.org/extensions/xep-0223.html)
	- Hinweise zur Überprüfung der Ereignisherkunft hinzufügen (als Reaktion auf CVE-2023-28686). (ka)

- [Version 1.1.0 von XEP-0313 (Verwaltung von Nachrichtenarchiven)](https://xmpp.org/extensions/xep-0313.html)
	- Klärung der Antwort auf Archiv-Metadaten im Falle eines leeren Archivs.
	- Klärung der Abfrage-Antwort, wenn es keine passenden Ergebnisse gibt. (mw)
	
### Letzter Aufruf

Letzte Aufrufe werden veröffentlicht, wenn alle mit dem aktuellen Stand des XEP zufrieden sind. Nachdem der Rat entschieden hat, ob der XEP fertig ist, veröffentlicht der XMPP-Redakteur einen Last Call für Kommentare. Das im Last Call gesammelte Feedback kann dazu beitragen, den XEP zu verbessern, bevor er an den Rat zurückgeschickt wird, damit er in den Status "Stable" überführt werden kann.

- Diesen Monat gibt es keinen Last Call.

### Stable

- In diesem Monat wurde kein XEP nach Stable verschoben.

### Abgelehnt

- In diesem Monat wurde kein XEP veraltet.

### Aufruf zur Erfahrung

Ein Call for Experience ist - wie ein Last Call - ein ausdrücklicher Aufruf zu Kommentaren, aber in diesem Fall richtet er sich vor allem an Personen, die die Spezifikation bereits implementiert und im Idealfall eingesetzt haben. Der Rat stimmt dann darüber ab, die Spezifikation als endgültig zu betrachten.

- Diesen Monat gibt es keinen Call for Experience.

## Verbreite die Neuigkeiten!

Bitte teile die Nachricht in anderen Netzwerken:

- [Mastodon](https://fosstodon.org/@xmpp/)
- [Twitter](https://twitter.com/xmpp)
- [YouTube](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
- [Lemmy-Instanz](https://community.xmpp.net/)
- [Reddit](https://www.reddit.com/r/xmpp/)
- [LinkedIn](https://www.linkedin.com/company/xmpp-standards-foundation/)

Schau dir auch unseren [RSS-Feed](https://xmpp.org/feeds/all.atom.xml) [EN] an!

Suchst du nach Stellenangeboten oder willst du einen professionellen Berater für dein XMPP-Projekt einstellen? Besuche unsere [XMPP-Jobbörse](https://xmpp.work/) [EN].

## Newsletter Mitwirkende & Übersetzungen
Dies ist eine Gemeinschaftsarbeit, und wir möchten uns bei den Übersetzern für ihre Beiträge bedanken. Freiwillige Helfer sind willkommen! Die Übersetzungen des XMPP-Newsletters werden hier veröffentlicht (mit etwas Verzögerung):

- Englisch (Original): [xmpp.org](https://xmpp.org/categories/newsletter/)
	- Allgemeine Mitwirkende: Adrien Bourmault (neox), Alexander "PapaTutuWawa", Arne, cal0pteryx, emus, Licaon_Kter, Ludovic Bocquet, melvo, MSavoritias (fae,ve), nicola, XSF iTeam

- Französisch: [jabberfr.org](https://news.jabberfr.org/category/newsletter/) und [linuxfr.org](https://linuxfr.org/tags/xmpp/public)
	- Übersetzer: Adrien Bourmault (neox), alkino, anubis, Benoît Sibaud, nyco, Pierre Jarillon, Ppjet6, Ysabeau

- Deutsch: [xmpp.org](https://xmpp.org/categories/newsletter/) und [anoxinon.de](https://anoxinon.de/blog/)
	- Übersetzer: Jeybe, wh0nix

- Italienisch: [notes.nicfab.eu](https://notes.nicfab.eu/)
	- Übersetzer: nicola

- Spanisch: [xmpp.org](https://xmpp.org/categories/newsletter/)
	- Übersetzer: daimonduff, TheCoffeMaker
	
## Hilf uns bei der Erstellung des Newsletters
Dieser XMPP-Newsletter wird von der XMPP-Community gemeinsam erstellt. Die monatliche Ausgabe des Newsletters wird in diesem [einfachen Pad](https://pad.nixnet.services/oHnY_ZvLT8SoFyCqIC2ung) [EN] verfasst. Am Ende eines jeden Monats wird der Inhalt des Blocks in das [XSF Github Repository](https://github.com/xsf/xmpp.org/milestone/3) [EN] eingefügt. Wir freuen uns immer über Mitwirkende. Zögere nicht, dich an der Diskussion in unserem [Comm-Team-Gruppenchat (MUC)](xmpp:commteam@muc.xmpp.org?join) [EN] zu beteiligen und uns dabei zu helfen, diese Arbeit als Gemeinschaftsprojekt aufrechtzuerhalten. Du hast ein Projekt und möchtest die Neuigkeiten verbreiten? Bitte ziehe in Erwägung, deine Neuigkeiten oder Veranstaltungen hier zu teilen und sie einem großen Publikum bekannt zu machen.

Aufgaben, die wir regelmäßig erledigen:

- das Sammeln von Neuigkeiten im XMPP-Universum
- kurze Zusammenfassungen von Neuigkeiten und Ereignissen
- Zusammenfassung der monatlichen Mitteilungen über Erweiterungen (XEPs)
- Überarbeitung des Newsletter-Entwurfs
- Vorbereitung von Medienbildern
- Übersetzungen
- Kommunikation über Medienkonten

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA-Lizenz](https://creativecommons.org/licenses/by-sa/4.0/) [EN] veröffentlicht.